package com.gitlab.htcgroup.events;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.smallrye.reactive.messaging.kafka.KafkaRecord;
import org.apache.kafka.common.header.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;

public class DomainEventHandler {
    
    private static final Logger LOG = LoggerFactory.getLogger(DomainEventHandler.class);
    
    protected final ObjectMapper objectMapper = new ObjectMapper();
    
    protected CompletionStage<Void> onMessage(
        KafkaRecord<String, String> message,
        Consumer<DomainEventEnvelope> cb
    ) {
        return CompletableFuture.runAsync(() -> {
            LOG.debug("Kafka message with key = {} arrived", message.getKey());
        
            String eventId = getHeaderAsString(message, "id");
            String eventType = getHeaderAsString(message, "eventType");
            JsonNode eventPayload = deserialize(message.getPayload());
            
            DomainEventEnvelope dee = new DomainEventEnvelope(eventId, eventType, eventPayload);
    
            if (msgAlreadyProcessed(dee)) {
                LOG.debug("Event with UUID {} was already retrieved, ignoring it", eventId);
                return;
            }
            
            cb.accept(dee);
        });
    }
    
    private String getHeaderAsString(KafkaRecord<?, ?> record, String name) {
        Header header = record.getHeaders().lastHeader(name);
        if (header == null) {
            throw new IllegalArgumentException("Expected record header '" + name + "' not present");
        }
        
        return new String(header.value(), StandardCharsets.UTF_8);
    }
    
    protected JsonNode deserialize(String event) {
        JsonNode eventPayload;
        
        try {
            eventPayload = objectMapper.readTree(event);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't deserialize", e);
        }
        
        return eventPayload.has("schema") ? eventPayload.get("payload") : eventPayload;
    }
    
    protected boolean msgAlreadyProcessed(DomainEventEnvelope dee) {
        return false;
    }
}
