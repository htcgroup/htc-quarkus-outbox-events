package com.gitlab.htcgroup.events;

import com.fasterxml.jackson.databind.JsonNode;

public record DomainEventEnvelope(
    String eventId,
    String type,
    JsonNode payload
) {}
